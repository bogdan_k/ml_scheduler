from flask import Flask
from app import plugins

import config


def init_app():
    # Initial app
    app = Flask(__name__)
    # Attach configs
    app.config.update(config)
    # Attach plugins
    plugins.init(app)
    # Return
    return app
