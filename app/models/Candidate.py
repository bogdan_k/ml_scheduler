
from app.plugins import sqla

class Candidate(sqla.Model):
    __tablename__ = 'candidates'

    id = sqla.Column(sqla.Integer, primary_key=True)
    name = sqla.Column(sqla.String(80), unique=False, nullable=False)
    lastname = sqla.Column(sqla.String(100), unique=False, nullable=False)
    email = sqla.Column(sqla.String(120), unique=True, nullable=False)
    is_lviv = sqla.Column(sqla.Boolean, nullable=False)
    status_cat = sqla.Column(sqla.Integer, nullable=False)
    js_cat = sqla.Column(sqla.Boolean, nullable=False)
    react_cat = sqla.Column(sqla.Boolean, nullable=False)
    node_cat = sqla.Column(sqla.Boolean, nullable=False)
    angular_cat = sqla.Column(sqla.Boolean, nullable=False)
    vue_cat = sqla.Column(sqla.Boolean, nullable=False)
    all_skills = sqla.Column(sqla.Float, nullable=False)
    salary = sqla.Column(sqla.Float, nullable=False)
    rating = sqla.Column(sqla.Integer, nullable=False)
    # meeting = db.relationship('Meeting', backref='candidate', lazy=True) 'to make inbase relation in the future'
