
from . import sqla
from . import migrate
from . import forms


def init(app):
    sqla.init_app(app)
    migrate.init_app(app, db=sqla)

