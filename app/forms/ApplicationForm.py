from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, FloatField
from wtforms.validators import DataRequired, Length

class ApplicationForm(FlaskForm):
    name = StringField(label='Name', validators=[DataRequired(), Length(min=3, max=15)] )
    lastname = StringField(label='Last Name', validators=[DataRequired(), Length(min=3, max=15)])
    email = StringField(label='Email', validators=[DataRequired(), Length(min=3, max=15)])
    is_lviv = BooleanField(label='Are you from Krakow?')
    status = BooleanField(label='Are you interested in job?')
    js = BooleanField(label='JS skill')
    react = BooleanField(label='React skill')
    node = BooleanField(label='Node skill')
    angular = BooleanField(label='Angular skill')
    vue = BooleanField(label='Vue skill')
    other = FloatField(label='Number of other skills, 0..18', validators=[DataRequired()])
    salary = FloatField(label='Expected Salary', validators=[DataRequired()])
